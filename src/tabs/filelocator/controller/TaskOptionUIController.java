
/*
 * Copyright (c) 2013, Alvin Cris Tabontabon
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
 * are met:
 *
 *     • Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 *     • Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in 
 *      the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tabs.filelocator.controller;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.WindowEvent;
import tabs.filelocator.FileBean;
import tabs.filelocator.Main;
import tabs.filelocator.SearchService;

/**
 * Controller class for every task.
 */
public class TaskOptionUIController implements Initializable {
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Create new instance of the service in every task.
        searchService = new SearchService();
                
        //Get the default directory
        cbDirectory.setItems(Main.getInstance().getDriveList());
        
        //Initialize table property
        colFileName.setCellValueFactory(new PropertyValueFactory<FileBean, String>("fileName"));
        colLocation.setCellValueFactory(new PropertyValueFactory<FileBean, String>("location"));
        colSize.setCellValueFactory(new PropertyValueFactory<FileBean, String>("size"));
        colDateCreated.setCellValueFactory(new PropertyValueFactory<FileBean, Date>("dateCreated"));
        
        //Remove center text of the table
        tblView.setPlaceholder(new Label(""));

        //Bind components to the running search service
        tblView.itemsProperty().bind(searchService.valueProperty());
        lblSearchStatus.textProperty().bind(searchService.messageProperty());
        btnStart.disableProperty().bind(searchService.runningProperty());
        btnStop.disableProperty().bind(searchService.runningProperty().not());
        screen.visibleProperty().bind(searchService.runningProperty());
        progress.visibleProperty().bind(searchService.runningProperty());
        
        // sets the behavior of the context menu
        setContextMenu();
    }    
 
    
    //---------------------------------------------------------- PRIVATE METHODS
    
    private void disableContextItems(boolean val, MenuItem... mi) {
        for (int i = 0; i < mi.length; i++) {
            mi[i].setDisable(val);
        }
    }
    
    private void setContextMenu() {
        context.setOnShowing(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                if (tblView.getSelectionModel().getSelectedIndex() == -1) {
                    disableContextItems(true, miCopy, miOpen, miOpenLocation);
                } else {
                    disableContextItems(false, miCopy, miOpen, miOpenLocation);
                }
            }
        });
    }
    
    private void addPopup(String message) {
        lblPopup.setText(message);
        lblPopup.setTooltip(new Tooltip(message));
        popupPane.setVisible(true);
    }
    
    
    //------------------------------------------------------------ EVENT HANDLER

    @FXML private void find(ActionEvent evt) {
        try {
            if (!txtToSearch.getText().trim().equals("")) {
                searchService.setPattern(txtToSearch.getText());
                searchService.setStartPath(Paths.get(cbDirectory.getSelectionModel().getSelectedItem()));
                searchService.run();
            } else {
                txtToSearch.requestFocus();
            }
        } catch (Exception ex) {
            addPopup("Please provide a valid directory to continue.");
        }
    }
    
    @FXML protected void stop(ActionEvent evt) {
        searchService.stop();
    }
    
    @FXML private void open(ActionEvent evt) {
        int i = tblView.getSelectionModel().getSelectedIndex();
        try {
            Desktop.getDesktop().open(new File(colLocation.getCellData(i).toString() + "\\" + colFileName.getCellData(i).toString()));
        } catch (IOException | IllegalArgumentException ex) {
            addPopup(ex.getMessage());
        }
    }
    
    @FXML private void openFileLocation(ActionEvent evt) throws IOException {
        int i = tblView.getSelectionModel().getSelectedIndex();
        Desktop.getDesktop().open(new File(colLocation.getCellData(i).toString()));
    }

    @FXML private void copyFile(ActionEvent evt) {
        int i = tblView.getSelectionModel().getSelectedIndex();
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        List<File> list = new ArrayList<>();
        
        list.add(new File(colLocation.getCellData(i).toString() + "\\" + colFileName.getCellData(i).toString()));
        
        clipboard.setContents(new FileSelection(list), null);
    }
    
    @FXML private void hidePopup(ActionEvent evt) {
        popupPane.setVisible(false);
    }
    
    /**
     * This nested class is used to allow the selected file to store in the 
     * system clipboard.
     */
    private class FileSelection implements Transferable {
        
        private List<File> files;
        
        public FileSelection(List<File> files) {
            this.files = files;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[]{DataFlavor.javaFileListFlavor};
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return DataFlavor.javaFileListFlavor.equals(flavor);
        }

        @Override
        public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
            if (!DataFlavor.javaFileListFlavor.equals(flavor)) {
                throw new UnsupportedFlavorException(flavor);
            }
            return files;
        }
        
    }
    
    //-------------------------------------------------------------- DECLARATION
    @FXML private TextField txtToSearch;
    @FXML private ComboBox<String> cbDirectory;
    @FXML private TableView tblView;
    @FXML private TableColumn colFileName;
    @FXML private TableColumn colLocation;
    @FXML private TableColumn colSize;
    @FXML private TableColumn colDateCreated;
    @FXML private Label lblSearchStatus;
    @FXML private Label lblPopup;
    @FXML private Button btnStart;
    @FXML private Button btnStop;
    @FXML private Region screen;
    @FXML private HBox popupPane;
    @FXML private ProgressIndicator progress;
    @FXML private ContextMenu context;
    @FXML private MenuItem miOpen;
    @FXML private MenuItem miOpenLocation;
    @FXML private MenuItem miCopy;

    private SearchService searchService;
    
}
