
/*
 * Copyright (c) 2013, Alvin Cris Tabontabon
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
 * are met:
 *
 *     • Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 *     • Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in 
 *      the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tabs.filelocator.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import tabs.filelocator.Main;

/*
 * Controller class for the main user interface.
 */
public class FileLocatorUIController implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Add default tab to the Tab Pane
        try { addTask(null); } 
        catch (IOException ex) {  }
    }    
    
    @FXML private void addTask(ActionEvent evt) throws IOException {
        Tab tab = new Tab("Task " + (taskPane.getTabs().size()+1));
        
        final FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/tabs/filelocator/ui/TaskOptionUI.fxml"));
        tab.setContent((BorderPane) loader.load());
        
        // Set an event handler for every Tab to prevent thread leak upon closing 
        // Tab while the search service is running.
        tab.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
                ((TaskOptionUIController) loader.getController()).stop(null);
            }
        });

        // Sets the default tab as unclosable.
        if (taskPane.getTabs().size() == 0) {
            tab.setClosable(false);
        }

        taskPane.getTabs().add(tab);
        taskPane.getSelectionModel().select(tab);
    }
    
    @FXML private void showAbout(ActionEvent evt) {
        aboutPane.setVisible(true);
    }

    @FXML private void closeAboutPane(ActionEvent evt) {
        aboutPane.setVisible(false);
    }
    
    @FXML private void close(ActionEvent evt) {
        Main.getInstance().close();
    }
    
    @FXML private void iconified(ActionEvent evt) {
        Main.getInstance().iconified();
    }
    
    //-------------------------------------------------------------- DECLARATION
    @FXML private TabPane taskPane;
    @FXML private AnchorPane aboutPane;
}
