
/*
 * Copyright (c) 2013, Alvin Cris Tabontabon
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
 * are met:
 *
 *     • Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 *     • Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in 
 *      the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


package tabs.filelocator;

import java.io.File;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Main class that will start the application and will set the FileLocator user
 * interface.
 */
public class Main extends Application {

    private double initX;       // X-axis coordinate of the Main UI
    private double initY;       // Y-axis coordinate of the Main UI
    private static Main instance;
    private Stage stage;
    private ObservableList<String> driveList = FXCollections.observableArrayList();

    @Override
    public void start(final Stage primaryStage) throws Exception {
        // Stores the instance of this class
        instance = this;
        
        // Create instance of the stage
        stage = primaryStage;
        
        // Loads the default directory
        loadDefaultDirectory();
        
        Parent facade = FXMLLoader.load(getClass().getResource("/tabs/filelocator/ui/FileLocatorUI.fxml"));
        Scene scene = new Scene(facade, Color.TRANSPARENT);

        facade.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                initX = me.getScreenX() - primaryStage.getX();
                initY = me.getScreenY() - primaryStage.getY();
            }
        });

        facade.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                primaryStage.setX(me.getScreenX() - initX);
                primaryStage.setY(me.getScreenY() - initY);
            }
        });

        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.setTitle("File Locator");
        primaryStage.show();
    }
    
    //----------------------------------------------------------- PUBLIC METHODS

    public static Main getInstance() {
        return instance;
    }
    
    public ObservableList<String> getDriveList() {
        return driveList;
    }

    public void close() {
        stage.close();
    }

    public void iconified() {
        stage.setIconified(true);
    }
    
    //---------------------------------------------------------- PRIVATE METHODS
    
    private void loadDefaultDirectory() {
        File[] roots = File.listRoots();
        for (int i = 0; i < roots.length; i++) {
            driveList.add(roots[i].toString());
        }
        driveList.add(System.getProperty("user.home"));
    }
}
