
/*
 * Copyright (c) 2013, Alvin Cris Tabontabon
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
 * are met:
 *
 *     • Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 *     • Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in 
 *      the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package tabs.filelocator;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/**
 * Service class that will provide the functionality to walk in the file tree
 * to find the missing files. 
 */
public class SearchService extends Service<ObservableList<FileBean>> {    
    
    public SearchService() {
        isStarted = false;
        terminated = false;
    }

    @Override
    protected Task<ObservableList<FileBean>> createTask() {
        return new SearchTask();
    }
    
    public void run() {
        if (isStarted) {
            restart();
            terminated = false;
        }
        else {
            start();
            isStarted = true;
        }    
    } 

    public void setPattern(String pattern) {
        matcher = FileSystems.getDefault().getPathMatcher("glob:*" + pattern + "*");
    }

    public void setStartPath(Path startPath) {
        this.startPath = startPath;
    }
    
    public void stop() {
        terminated = true;
    }

    private class SearchTask extends Task<ObservableList<FileBean>> {

        @Override
        protected ObservableList<FileBean> call() throws Exception {
            final ObservableList<FileBean> foundPaths = FXCollections.observableArrayList();
            
            try {
                Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        // Check if the user has stopped the running service
                        if (terminated)
                            return FileVisitResult.TERMINATE;
                       
                        if (matcher.matches(file.getFileName())) {
                            foundPaths.add(new FileBean(file.getFileName().toString(), file.getParent().toString(),
                                attrs.size() + " bytes", new Date(attrs.creationTime().toMillis())));
                        }
                        updateMessage("Found: " + foundPaths.size() + "\t|\tSearching: " + file);
            
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (Exception e) {
                updateMessage("Error: " + e.getMessage());
            } finally {
                if (foundPaths.size() == 0) 
                    updateMessage("No files found.");
                else {
                    updateMessage("Found: " + foundPaths.size() + " file(s).");
                }
            }

            return foundPaths;
        }
    }
    
    //------------------------------------------------------------- DECLARATIONS
    
    // PatchMatcher object that will check if the user pattern is matched.
    private PathMatcher matcher;
    
    // This variable holds the start directory of search service.
    private Path startPath;
    
    // This variable checks if the Search service is already started.
    private boolean isStarted;
    
    // This variable allows user to terminate running service.
    private boolean terminated;
}
